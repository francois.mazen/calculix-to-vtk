from vtk import vtkUnstructuredGrid
from vtk import vtkXMLUnstructuredGridWriter
from vtk import vtkPoints
from vtk import vtkDoubleArray
from vtk import VTK_QUADRATIC_HEXAHEDRON


# Read data.
data = {}
with open("result.frd", 'r') as f:
    line = f.readline()
    while line:
        if line[0:6] == '    1C':
            data['calc name'] = line[6:]

        elif line[0:6] == '    1U':
            if not 'user informations' in data:
                data['user informations'] = []
            data['user informations'].append(line[6:])

        elif line[0:6] == '    2C':
            number_of_nodes = int(line[7:37])
            node_format = int(line[38:])
            if node_format != 1:
                raise("Node long format (indicator 1) supported only.")
            for node_index in range(number_of_nodes):
                line = f.readline()
                if line[0:3] != ' -1':
                    raise("Wrong record in node section: the line does not start with ' -1'.")
                node_number = int(line[3:13])
                node_x = float(line[13:25])
                node_y = float(line[25:37])
                node_z = float(line[37:49])
                if not 'nodes' in data:
                    data['nodes'] = []
                data['nodes'].append([node_number, [node_x, node_y, node_z]])
            line = f.readline()
            if line[0:3] != ' -3':
                raise("Missing end record for node block: the line does not start with ' -3'.")

        elif line[0:6] == '    3C':
            number_of_elements = int(line[7:37])
            element_format = int(line[38:])
            if element_format != 1:
                raise("Element long format (indicator 1) supported only.")
            line = f.readline()
            for element_index in range(number_of_elements):
                if line[0:3] != ' -1':
                    raise("Wrong record in element section: the line does not start with ' -1'.")
                element_number = int(line[3:13])
                element_type = int(line[13:18])
                element_group = int(line[18:23])
                element_material = int(line[23:28])
                node_numbers = []
                line = f.readline()
                while line[0:3] == ' -2':
                    if len(line) > 13:
                        node_numbers.append(int(line[3:13]))
                    if len(line) > 13:
                        node_numbers.append(int(line[13:23]))
                    if len(line) > 13:
                        node_numbers.append(int(line[23:33]))
                    if len(line) > 13:
                        node_numbers.append(int(line[33:43]))
                    if len(line) > 13:
                        node_numbers.append(int(line[43:53]))
                    if len(line) > 13:
                        node_numbers.append(int(line[53:63]))
                    if len(line) > 13:
                        node_numbers.append(int(line[63:73]))
                    if len(line) > 13:
                        node_numbers.append(int(line[73:83]))
                    if len(line) > 13:
                        node_numbers.append(int(line[83:93]))
                    if len(line) > 13:
                        node_numbers.append(int(line[93:103]))
                    line = f.readline()
                if not 'elements' in data:
                    data['elements'] = []
                data['elements'].append([element_number, element_type, element_group, element_material, node_numbers])
            if line[0:3] != ' -3':
                print(line[0:3])
                raise("Missing end record for element block: the line does not start with ' -3'.")

        elif line[0:6] == '  100C':
            number_of_values = int(line[24:36])

            line = f.readline()
            if line[0:3] != ' -4':
                raise("Missing array info record for node block: the line does not start with ' -4'.")
            name = line[3:17].strip()

            number_of_components = 0
            line = f.readline()
            while line[0:3] == ' -5':
                if line[5:8] != 'ALL':
                    number_of_components = number_of_components + 1
                line = f.readline()
                array = {}
                array[name] = []

            for value_index in range(number_of_values):
                if line[0:3] != ' -1':
                    raise("Wrong record in data section: the line does not start with ' -1'.")
                node_number = int(line[3:13])
                values = []

                for comp in range(number_of_components):
                    values.append(float(line[(12*comp+13):(12*(comp+1)+13)]))

                array[name].append([node_number, values])
                line = f.readline()

            data['arrays'] = array

            # Stop reading here for this example
            break

        line = f.readline()

# Create VTK data object.
my_vtk_dataset = vtkUnstructuredGrid()

# Create points.
points = vtkPoints()
nodes = data['nodes']
node_map = {}

for node_id in range(len(nodes)):
    points.InsertPoint(node_id, nodes[node_id][1])
    node_map[nodes[node_id][0]] = node_id

my_vtk_dataset.SetPoints(points)

# Create cells.
elements = data['elements']
my_vtk_dataset.Allocate(len(elements))

for element_id in range(len(elements)):
    if elements[element_id][1] != 4:
        raise("Unsupported element type, he20 element supported only (type 4).")

    nodes = [node_map[id] for id in elements[element_id][4]]
    node_numbers = [nodes[0],nodes[1],nodes[2],nodes[3],
                    nodes[4],nodes[5],nodes[6],nodes[7],
                    nodes[8],nodes[9],nodes[10],nodes[11],
                    nodes[16],nodes[17],nodes[18],nodes[19],
                    nodes[12],nodes[13],nodes[14],nodes[15]]
    my_vtk_dataset.InsertNextCell(VTK_QUADRATIC_HEXAHEDRON, 20, node_numbers)

# Create data array.
name = list(data['arrays'].keys())[0]
array_values = data['arrays'][name]

array = vtkDoubleArray()
array.SetNumberOfComponents(3)
array.SetNumberOfTuples(len(array_values))
array.SetName(name)

for values in array_values:
    array.SetTuple(values[0] - 1, values[1])

my_vtk_dataset.GetPointData().AddArray(array)

# Write VTK data object.
writer = vtkXMLUnstructuredGridWriter()
writer.SetFileName("output.vtu")
writer.SetInputData(my_vtk_dataset)
writer.Write()
